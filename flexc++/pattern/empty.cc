#include "pattern.ih"

bool Pattern::empty(set<size_t> &indices, States const &states, size_t idx)
{
    State const &state = states[idx];

    switch (state.type())
    {
        case EMPTY:
            indices.insert(idx);
        
            if (indices.find(state.next1()) != indices.end() or 
                indices.find(state.next2()) != indices.end()
            )
                fmsg << 
                    "Internal error: specified scanner regexes cause "
                    "flexc++'s parser to recursively check grammar state " << 
                    idx << ". This error is commonly caused by scanner "
                    "regexes that can be simplified without loss of meaning "
                    "(e.g., '.*+' can be simplified to '.*').";
        
        return empty(indices, states, state.next1()) or 
               empty(indices, states, state.next2());

        case FINAL:
        return true;

        default:
        return false;
    }
}

