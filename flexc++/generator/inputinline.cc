#include "generator.ih"

void Generator::inputInline(std::ostream &out) const
{
    string const &filename = d_options.inputInline();

    key(out);

    if (not filename.empty())           // user-provided input filename
        out << "#include \"" << filename << "\"\n";
    else
    {
        string const &className = d_options.className();

        out <<
R"(
inline size_t )" << className << R"(Base::Input::lineNr() const
{
    return d_lineNr;
}
inline size_t )" << className << R"(Base::Input::nPending() const
{
    return d_deque.size();
}
inline void )" << className << R"(Base::Input::setPending(size_t size)
{
    d_deque.erase(d_deque.begin(), d_deque.end() - size);
}
inline void )" << className << R"(Base::Input::close()
{
    delete d_in;
    d_in = 0;                   // switchStreams also closes
}

)";
// >>>> R"(...)" section ends <<<<
    }
}







