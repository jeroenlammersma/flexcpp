#include "generator.ih"

void Generator::scannerConstructors(ostream &out) const
{
    key(out);

    std::string const &name = d_options.className();

    out << 
        "inline " << name << "::" << name << 
                     "(std::istream &in, std::ostream &out, bool keepCwd)\n"
        ":\n"
        "    " << name << "Base(in, out, keepCwd)\n"
        "{}\n";

    if (not d_options.interactive())
        out <<
            "\n"
            "inline " << name << "::" << name << 
                "(std::string const &infile, std::string const &outfile, "
                                            "bool keepCwd)\n"
            ":\n"
            "    " << name << "Base(infile, outfile, keepCwd)\n"
            "{}\n";
}
