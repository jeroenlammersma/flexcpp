#include "generator.ih"

namespace 
{

char const interactive[] = R"_(    d_in(&in),
    d_line(new std::istringstream()),
    d_dfaBase_(s_dfa_)
{
    if (keepCwd)
        d_cwd = std::filesystem::current_path().string();
    p_pushStream(s_istream, new std::istream(d_line->rdbuf()));
}
)_";

char const nonInteractive[] = R"_(    d_in(0),
    d_dfaBase_(s_dfa_)
{
    if (keepCwd)
        d_cwd = std::filesystem::current_path().string();
    p_pushStream(s_istream, new std::istream(in.rdbuf()));
}
)_";

} // namespace

void Generator::interactiveInit(ostream &out) const
{
    key(out);

    out << (d_options.interactive() ? interactive : nonInteractive);
}




