//XERR
#include "state.ih"

State::State(unsigned char ch, size_t next1)
:
    State(ch, new StateData(next1, 0))
{
    xerr((size_t)this % 1000 << ": d_data initialization by delegation");
}

