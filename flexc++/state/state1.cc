//#define XERR
#include "state.ih"

State::State()
:
    d_type(UNDETERMINED_),
    d_rule(-1)
{
    xerr((size_t)this % 1000 << ": State() -> d_data = 0");
}
